package com.webdriver.tests;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverTest {
    private static WebDriver driver;
    @Test
    public static void setUp(){
        driver = new ChromeDriver();
        driver.findElement(By.name("email")); //login
        driver.findElement(By.name("pass")); //password
        driver.findElement(By.id("u_0_5")); //login button
        driver.findElement(By.className("_mp3 _38vo")); //avatar
        driver.findElement(By.className("_2n_9")); //add to friends button
        driver.findElement(By.className("_1dwg _1w_m")); //second post
        driver.findElement(By.className("img sp_57uLR4yLzBk sx_e24bc4")); //photo
    }
}
